---
layout: post
title: "Build, Dockerize, and Deploy your GitLab Repositories with Distelli"
date: 2016-04-21 12:00
comments: true
author: Alex Leventer from Distelli
author_twitter: alexleventer
image_title: /images/gitlabintegrationimage.jpg
---
*This is a guest post by Distelli.*

Here at Distelli, we're obsessed with simplifying DevOps so developers can focus on product features, not infrastructure automation.

Today, we're excited to announce the release of the Distelli/GitLab integration. The integration makes it extremely simple to continuously deliver your application from GitLab to any server in the world. Simply push to GitLab to trigger a build, Dockerize, and deploy your application in minutes.

<!-- more -->

Below I'll demonstrate how simple it is to automate the build, test and deployment of a simple Node.js GitLab repository using Distelli.

## How to connect GitLab and Distelli
Connecting GitLab and Distelli takes a matter of minutes.

### Create a Distelli account:
[Head over to Distelli](https://www.distelli.com/signup) and create an account.

![Alt text](https://monosnap.com/file/kWb6z883vEYl1O8uicmUWHE63zlh7x.png)

### Create an application:
Once you’ve created an account, you’ll automatically be redirected to create your first application. Select the blue **Connect GitLab** button.

![Alt text](https://monosnap.com/file/UQDqZcEB9mP5a2T5eiYETHKh66wBi8.png)

### Add GitLab credentials:
You'll be prompted to add your GitLab host URL and API token. If you're using the hosted version of GitLab, enter `https://gitlab.com`. Your token can be found in the **Profile Settings** > **Account** section of your GitLab account. Once you've added your credentials, click the blue **Add Credentials** button.

![Alt text](https://monosnap.com/file/sL8WxL55g59ALRZAtxlKU9CSmPUyVh.png)

### Select a GitLab organization:
Next, you'll need to select a GitLab organization. I'll select my personal account.

![Alt text](https://monosnap.com/file/7hh71NwhZ0gKcbip2y2YVQ8yPuYWNm.png)
	
### Select a repository:
Select a GitLab repository to associate with your Distelli application. I'll select a Hello-World Node.js repository I created for this example.

![Alt text](https://monosnap.com/file/xVfa4q9mi57yZi3DVX1j5DbchomZDP.png)

### Set build triggers:
Next, you'll need to set build triggers or rules on which branches Distelli should build. For this step, you can manually select branches or set a regular expression to add branches automatically. I'm going to configure Distleli to only build on changes to my **master** branch.

![Alt text](https://monosnap.com/file/dByksv1CDe0tIPNyGWb4NpQRUknFgM.png)

### Set build steps:
Now, we need to set our build steps. I'll add an `npm install` command in my build step to install all my Node.js package dependencies.

![Alt text](https://monosnap.com/file/rxud37AewAJIscJUibvY2KWPFU7MkI.png)

### Start your first build:
Finally, I'll select the Javascript build image and start my first build by clicking the blue **Looks Good. Start Build!** button. This tells Distelli to build my application on one of Distelli's hosted build servers inside of a Docker container with Distelli's Javascript image.

![Alt text](https://monosnap.com/file/chOIY9tgcrGF2xInfzwNujOs8MLrMP.png)

Below, we can see our first build is in-progress. The GitLab commit is linked below the commit message.

![Alt text](https://monosnap.com/file/d7CeFLcsHV4KF9OxKY4Fq3mxew8EmH.png)

### Setting up a pipeline:
A new release of my application is created anytime a build succeeds. I specify in my PkgInclude section what files to include in my release. Now that we've created a release, let's set up an application pipeline. Click on your application's name:

![Alt text](https://monosnap.com/file/HUDflinbRh67G8MKRVi0kmx3gjvUwZ.png)

Let's set up Distelli to automatically deploy to our Beta environment whenever a build succeeds. Click the **Add Step** button on the bottom right of the page:

![Alt text](https://monosnap.com/file/wQ3SQMTiuBpHlIi6yhSeB1mxDUmlYi.png)

I've set up my Beta environment to automatically be deployed to whenever a build succeeds.

![Alt text](https://monosnap.com/file/xH6V2DoJUFhFdTgqigLXw0AMMwcyLE.png)

Now let's rebuild our GitLab repo and test that the our application is automatically deployed when the build succeeds. Click the blue wrench icon at the top right of the page to rebuild.

![Alt text](https://monosnap.com/file/18QLf3AxxlbR8nBoqBMllwtDRWsOMS.png)

We can see right from the application pipeline that our build is in progress.

![Alt text](https://monosnap.com/file/t5MFr6J17w5pGASMLd4E4OmHhTZfBT.png)

The application has been successfully deployed! We can click on the deployment number to see additional information on the deploy.

![Alt text](https://monosnap.com/file/wAkxM4OuR4lOlhax8BpqbcHxHcrp5T.png)

Questions about Distelli? Shoot us an email at [support@distelli.com](mailto:support@distelli.com). 

